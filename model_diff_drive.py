# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 15:18:45 2016

@author: kkalem
"""

import numpy as np
import view_plot as vp

class LawnMower():
    def __init__(self,X=[0.,0.],V=[0.,0.], max_v = 100., max_omega = np.pi/4, size = 1.):
        #position of the model in 2D, do we need 3D?
        self.X = np.array(X)
        #velocity of the model, the rotation of the vector is the current
        #heading of the model and the length of the vector is its lineer velocity
        self.V = np.array(V)
        #maximum linear velocity
        self.max_v = max_v
        #maximum angular velocity
        self.max_omega = max_omega
        #size of the model as a disk radius
        self.size = size

        #current time in simulation ticks
        self.time = 0
        #the time lenght in seconds each 'tick' in simulation is
        self.dt = 0.03


        #current heading of the model
        self.theta = np.arctan2(self.V[1],self.V[0])
        #desired angular velocity
        self.omega = 0.
        #desired linear velocity
        self.v = 0.

        #the target this model is trying to reach
        #initially the target is its own position
        self.target = self.X

        #list of targets this model will try to follow
        self.target_list = [self.target]

    def tick(self):
        ##TODO apply your controls here
        self.__doPhysics()

    def setTarget(self,T):
        self.target = np.array(T)

    def addTarget(self,T,multi=False):
        if multi:
            for t in T:
                self.target_list.append(np.array(t))
        else:
            self.target_list.append(np.array(T))


    def __doPhysics(self):
        #new heading of the model
        self.theta += self.omega
        #new velocity of the model
        self.V = np.array([self.v*self.dt*np.cos(self.theta),self.v*self.dt*np.sin(self.theta)])
        #new position of the model
        #the reason to mutate the existing X vector is to be able to have easy plotting
        self.X += self.V

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    m = LawnMower(X = [25.,25.])
    v = vp.PlotView(m)
    plt.scatter([0,0,50,50],[0,50,0,50])

    m.omega = np.pi/20
    m.v = 10
    m.setTarget([10,0])
    plt.scatter(m.target[0],m.target[1],marker = 'x')

    for time in range(100):
        v.update()

